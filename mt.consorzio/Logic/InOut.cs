﻿using System;
using System.Terminal;

namespace mt.consorzio.Logic
{
    public static class InOut
    {
        private static int _signalRedPin = 5;
        private static int _signalGreenPin =4;
        
        public enum ScaleState
        {
            Empty,
            Occupied,
            ReadyToLeave
        }

        #region Signal Red

       
        private static bool Out_SignalRed
        {
            set
            {
                IO.Set(_signalRedPin, value);
            }
        }
        #endregion

        #region Signal Green

       
        private static bool Out_SignalGreen
        {
            set
            {
                IO.Set(_signalGreenPin, value);
            }
        }

        #endregion

        public static void SetScaleBusy(ScaleState scaleBusy)
        {
            switch (scaleBusy)
            {
                case ScaleState.Empty:
                    Out_SignalRed = false;
                    Out_SignalGreen = false;
                    break;
                case ScaleState.Occupied:
                    Out_SignalRed = true;
                    Out_SignalGreen = false;
                    break;
                case ScaleState.ReadyToLeave:
                    Out_SignalRed = false;
                    Out_SignalGreen = true;
                    break;
            }

        }
    }
}
