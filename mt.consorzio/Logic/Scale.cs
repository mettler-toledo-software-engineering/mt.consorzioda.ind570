﻿using System;
using System.Terminal;

namespace mt.consorzio.Logic
{
    public static class Scale
    {
        [Share("pt0101")]
        public static string Template1;

        //[Share("wc0104")]
        //private static int zeroScale;

        //[Share("wx0104")]
        //private static int zeroStatus;

        //[Share("wc0101")]
        //private static int tareScale;

        [Share("wc0102")]
        private static int clrTareScale;

        //[Share("wx0101")]
        //private static int tareStatus;

        [Share("ws0102")]
        private static double tareWeight;

        [Share(SD.Scale.Status, 31)]
        private static bool inMotion;

        [Share(SD.DisplayAndKeyboard.KeyboardRoutingCommands, 14)]
        public static int RouteScaleKeys;

        [Share(SD.DisplayAndKeyboard.KeyboardRoutingCommands, 15)]
        public static int RouteClearTareKey;

        // for test
        //[Share(SD.Application.ProcessString, 1)]
        //private static string wgt;

        [Share(SD.Application.ProcessString, 3)]
        public static string WeightFormatLeftMargin;

        [Share(SD.Application.ProcessString, 4)]
        public static string WeightFormatRightMargin;

        //[Share(SD.Scale.DynamicWeight, 1)]
        //private static string grossWeightStr;

        [Share(SD.Scale.DynamicWeight, 10)]
        private static double grossWeight;

        [Share(SD.Scale.Calibration, 5)]
        private static double lowIncrement;

        [Share(SD.Scale.DynamicWeight, 3)]
        public static string Unit;

        [Share(SD.Scale.Commands, 03)] // print command
        private static int aprint;

        [Share(SD.Scale.Status, 03)] // print command
        private static int pStatus;

        //[Share(SD.PrintAndTemplateData.DemandPrintSetup, 02)] // print command
        //private static bool noMotionBeforePrinting;

        [Share(SD.System.MonitoringAndServiceData, 1)]
        private static double tCounter;

        private static double weight;

        public static double ManTare;

        public static void Initialize()
        {
            string temp;
          //  Template1 = "!/wt0101\";\"!/X1/!/wt0102\";\"!/X1/!/ws0110\";\"!/xp0101!/E0";

            WeightFormatLeftMargin = lowIncrement.ToString().TrimStart();
            temp = "";
            for (int i = 1; i <= WeightFormatLeftMargin.Length; i++)
            {
                temp += (WeightFormatLeftMargin.Substring(i, 1) == "." ? "." : "#");
            }
            WeightFormatLeftMargin = temp;
            WeightFormatRightMargin = temp.PadLeft(7, "#");
            ClearTare();
        }

        public static string ScaleFormat(double w)
        {
            string s = ScaleFormatWithoutUnit(w);
            return s + " " + Scale.Unit;
        }

        public static string ScaleFormatWithoutUnit(double w)
        {
            string s;
            File file = new File(1);
            file.Open("f", FileAccess.Output);
            file.WriteFormat(Scale.WeightFormatLeftMargin, w);
            file.Close();
            File fi = new File(1);
            fi.Open("f", FileAccess.Input);
            fi.ReadLine(out s);
            fi.Close();
            return s;
        }
        public static double TransCounter
        {
            [Inline]
            get
            {
                return tCounter - 1;
            }
        }

        public static void AlibiPrint()
        {
            //noMotionBeforePrinting = false;
            while (inMotion)
                Task.Sleep(100);
            aprint = 1;
            while (aprint == 1) Task.Sleep(100);
            while (pStatus == 1)
                Task.Sleep(100);
            weight = grossWeight;
        }

        public static bool InMotion
        {
            [Inline]
            get
            {
                return inMotion;
            }
        }


        public static double GrossIstWeight
        {
            [Inline]
            get
            {
                return weight;
            }
        }
        public static double GrossWeight
        {
            [Inline]
            get
            {
                return grossWeight;
            }
        }

        public static double TareWeight
        {
            [Inline]
            get
            {
                return tareWeight;
            }
        }

                //public static int SetZero()
        //{
        //    zeroScale = 1;
        //    do
        //    {
        //        Task.Sleep(100);
        //    }
        //    while (zeroStatus == 1 || zeroScale == 1);
        //    return zeroStatus;
        //}

        //public static bool SetTare()
        //{
        //    tareScale = 1;
        //    do
        //    {
        //        Task.Sleep(100);
        //    }
        //    while (tareStatus == 1 || tareScale == 1);
        //    return tareStatus == 0;
        //}

        public static void ClearTare()
        {
            ManTare = 0;
            //PresetTare = 0;
            if (tareWeight > 0)
                clrTareScale = 1;
        }
    }
}
