﻿using System;
using System.Terminal;
using mt.consorzio.Configuration;

namespace mt.consorzio.Logic
{
    public static class TcpClient
    {
        private static Socket client;

        private static bool Connect()
        {
            //ActionState = "Verbinden...";

            client = Socket.Connect(Settings.HostIpAddress, Settings.HostPort);
            if (!client.IsValid)
            {
                //Display.Popup("TCPClient", "Keine Verbindung", "zum Server!");
                Display.Popup("TCPClient", "Nessuna connessione", "al server!");
                return false;
            }
            return true;
        }

        public static void Close()
        {
            client.Close();
        }


        #region GetDateTime

        public static void GetDateTime()
        {
            string result = "";
            string thisdate = "";

            if (Connect())
            {
                //ActionState = "Verbinden mit dem Server";
                client.Write("#XWindowsTools.CurrentDateTime?");
                client.Write(string.Chr(8));
                client.Write("dd.MM.yyyy HH:mm:ss");
                client.Write("\f");

                double tEnd = DeviceTimer.GetTicks() + 2 /* sec */;
                bool quit = false;
                do
                {
                    result += client.Read(50, 50);
                    if (result.IndexOf("\f") > 1)
                        quit = true;
                }
                while (!quit && tEnd > DeviceTimer.GetTicks());

                if (quit)
                {
                    // receive ok
                    TcpClient.Close();

                    int posStart = result.IndexOf("#S");
                    if (posStart > 0 && result.Length > 3)
                    {

                        string s = result.Substring(posStart + 2);
                        int length = s.IndexOf("\f") - 1;
                        thisdate = s.Substring(1, length);

                        // Zielformat yyyy.MM.dd 
                        string a = thisdate.Substring(7, 4) + ".";
                        string b = thisdate.Substring(4, 2) + ".";
                        string c = thisdate.Left(2);
                        Program.CurrrentDate = a + b + c;
                        // Format HH:mm:ss
                        Program.CurrrentTime = thisdate.Substring(12, 8);
                    }
                }
            }
        }

        #endregion

        #region GetCustomerData

        public static void GetCustomerData(string code, string id)
        {
            string result = "";

            if (Connect())
            {
                //ActionState = "Verbindung hergestellt";
                client.Write("#XConsorzioExtension.GetCustomerData??");
                client.Write(string.Chr(8));
                client.Write(code);
                client.Write(string.Chr(8));
                client.Write(id);
                client.Write("\f");

                double tEnd = DeviceTimer.GetTicks() + 2 /* sec */;
                bool quit = false;
                do
                {
                    result += client.Read(50, 50);
                    if (result.IndexOf("\f") > 1)
                        quit = true;
                }
                while (!quit && tEnd > DeviceTimer.GetTicks());

                if (quit)
                {
                    TcpClient.Close();

                    int posStart = result.IndexOf("#S");
                    if (posStart > 0 && result.Length > 3)
                    {
                        string s = result.Substring(posStart + 2);
                        int length = s.IndexOf(";") - 1;
                        Program.CustomerId = int.Parse(s.Substring(1, length));
                        s = s.Substring(length + 2);
                        length = s.IndexOf(";") - 1;
                        Program.Customer = s.Substring(1, length);
                        s = s.Substring(length + 2);
                        length = s.IndexOf(";") - 1;
                        Program.CustomerCode = s.Substring(1, length);
                    }
                    else
                    {
                        Program.CustomerId = 0;
                    }
                }
                else
                {
                    // timeout
                }
            }
        }

        #endregion

        #region GetArticleData

        public static void GetArticleData(string code, string id)
        {
            string result = "";

            if (Connect())
            {
                //ActionState = "Verbinden mit dem Server";
                client.Write("#XConsorzioExtension.GetArticleData??");
                client.Write(string.Chr(8));
                client.Write(code);
                client.Write(string.Chr(8));
                client.Write(id);
                client.Write("\f");

                double tEnd = DeviceTimer.GetTicks() + 2 /* sec */;
                bool quit = false;
                do
                {
                    result += client.Read(50, 50);
                    if (result.IndexOf("\f") > 1)
                        quit = true;
                }
                while (!quit && tEnd > DeviceTimer.GetTicks());

                if (quit)
                {
                    // receive ok
                    TcpClient.Close();

                    int posStart = result.IndexOf("#S");
                    if (posStart > 0 && result.Length > 3)
                    {

                        string s = result.Substring(posStart + 2);
                        int length = s.IndexOf(";") - 1;
                        Program.ArticleId = int.Parse(s.Substring(1, length));
                        s = s.Substring(length + 2);
                        length = s.IndexOf(";") - 1;
                        Program.Article = s.Substring(1, length);
                        s = s.Substring(length + 2);
                        length = s.IndexOf(";") - 1;
                        Program.ArticleCode = s.Substring(1, length);
                    }
                    else
                    {
                        Program.ArticleId = 0;
                    }
                }
                else
                {
                    // timeout
                }
            }
        }

        #endregion

        #region GetTransportData

        public static void GetTransportData(int id)
        {
            string result = "";

            if (Connect())
            {
                //ActionState = "Verbinden mit dem Server";
                client.Write("#XConsorzioExtension.GetTransportData?");
                client.Write(string.Chr(8));
                client.Write(id.ToString());
                client.Write("\f");

                double tEnd = DeviceTimer.GetTicks() + 2 /* sec */;
                bool quit = false;
                do
                {
                    result += client.Read(50, 50);
                    if (result.IndexOf("\f") > 1)
                        quit = true;
                }
                while (!quit && tEnd > DeviceTimer.GetTicks());

                if (quit)
                {
                    // receive ok
                    TcpClient.Close();

                    int posStart = result.IndexOf("#S");
                    if (posStart > 0 && result.Length > 3)
                    {
                        string s = result.Substring(posStart + 2);
                        int length = s.IndexOf(";") - 1;
                        Program.TransportId = int.Parse(s.Substring(1, length));
                        s = s.Substring(length + 2);
                        length = s.IndexOf(";") - 1;
                        Program.Transporter = s.Substring(1, length);
                    }
                    else
                    {
                        Program.TransportId = 0;
                    }
                }
                else
                {
                    // timeout
                }
            }
        }

        #endregion

        #region GetVehicleData

        public static void GetVehicleData(string vehicleCode)
        {
            string result = "";

            if (Connect())
            {
                //ActionState = "Verbinden mit dem Server";
                client.Write("#XConsorzioExtension.GetVehicleData?");
                client.Write(string.Chr(8));
                client.Write(vehicleCode);
                client.Write("\f");

                double tEnd = DeviceTimer.GetTicks() + 2 /* sec */;
                bool quit = false;
                do
                {
                    result += client.Read(50, 50);
                    if (result.IndexOf("\f") > 1)
                        quit = true;
                }
                while (!quit && tEnd > DeviceTimer.GetTicks());

                if (quit)
                {
                    // receive ok
                    TcpClient.Close();

                    int posStart = result.IndexOf("#S");
                    if (posStart > 0 && result.Length > 3)
                    {
                        string s = result.Substring(posStart + 2);
                        int length = s.IndexOf(";") - 1;
                        Program.VehicleId = int.Parse(s.Substring(1, length));
                        s = s.Substring(length + 2);
                        length = s.IndexOf(";") - 1;
                        Program.Vehicle = s.Substring(1, length);
                        s = s.Substring(length + 2);
                        length = s.IndexOf(";") - 1;
                        Program.TransportId = int.Parse(s.Substring(1, length));
                    }
                    else
                    {
                        Program.VehicleId = 0;
                    }
                }
                else
                {
                    // timeout
                }
            }
        }

        #endregion

        #region GetFirstWeighingData

        public static void GetFirstWeighingData(string weighingCode)
        {
            string result = "";

            if (Connect())
            {
                //ActionState = "Verbinden mit dem Server";
                client.Write("#XConsorzioExtension.GetFirstWeighingData?");
                client.Write(string.Chr(8));
                client.Write(weighingCode);
                client.Write("\f");

                double tEnd = DeviceTimer.GetTicks() + 2 /* sec */;
                bool quit = false;
                do
                {
                    result += client.Read(50, 50);
                    if (result.IndexOf("\f") > 1)
                        quit = true;
                }
                while (!quit && tEnd > DeviceTimer.GetTicks());

                if (quit)
                {
                    // receive ok
                    TcpClient.Close();

                    int posStart = result.IndexOf("#S");
                    if (posStart > 0 && result.Length > 3)
                    {
                        string s = result.Substring(posStart + 2);
                        int length = s.IndexOf(";") - 1;
                        Program.FirstWeightId = int.Parse(s.Substring(1, length));
                        s = s.Substring(length + 2);
                        length = s.IndexOf(";") - 1;
                        Program.FirstWeight = double.Parse(s.Substring(1, length));
                    }
                    else
                    {
                        Program.FirstWeight = 0;
                    }
                }
                else
                {
                    // timeout
                }
            }
        }

        #endregion

        #region GetTransactionData

        public static void GetTransactionData(int vehicleId)
        {
            //int customerId = -1;
            string result = "";

            if (Connect())
            {
                client.Write("#XConsorzioExtension.GetTransactionData?");
                client.Write(string.Chr(8));
                client.Write(vehicleId.ToString());
                client.Write("\f");

                double tEnd = DeviceTimer.GetTicks() + 2 /* sec */;
                bool quit = false;
                do
                {
                    result += client.Read(50, 50);
                    if (result.IndexOf("\f") > 1)
                        quit = true;
                }
                while (!quit && tEnd > DeviceTimer.GetTicks());

                if (quit)
                {
                    TcpClient.Close();

                    int posStart = result.IndexOf("#S");
                    if (posStart > 0 && result.Length > 3)
                    {
                        string s = result.Substring(posStart + 2);
                        int length = s.IndexOf(";") - 1;
                        Program.TransactionId = int.Parse(s.Substring(1, length));
                        s = s.Substring(length + 2);
                        length = s.IndexOf(";") - 1;
                        Program.ArticleId = int.Parse(s.Substring(1, length));
                        s = s.Substring(length + 2);
                        length = s.IndexOf(";") - 1;
                        Program.CustomerId = int.Parse(s.Substring(1, length));
                        s = s.Substring(length + 2);
                        length = s.IndexOf(";") - 1;
                        Program.FirstWeightId = int.Parse(s.Substring(1, length));
                    }
                    else
                    {
                        Program.TransactionId = 0;
                    }
                }
                else
                {
                    // timeout
                }
            }
        }

        #endregion

        #region GetWorkingState

        public static bool GetWorkingState()
        {
            string result = "";
            string resultString = "";

            if (Connect())
            {
                //ActionState = "Verbinden mit dem Server";
                client.Write("#XConsorzioExtension.GetWorkingState");
                client.Write("\f");

                double tEnd = DeviceTimer.GetTicks() + 2 /* sec */;
                bool quit = false;
                do
                {
                    result += client.Read(50, 50);
                    if (result.IndexOf("\f") > 1)
                        quit = true;
                }
                while (!quit && tEnd > DeviceTimer.GetTicks());

                if (quit)
                {
                    // receive ok
                    TcpClient.Close();

                    int posStart = result.IndexOf("#S");
                    if (posStart > 0 && result.Length > 3)
                    {
                        string s = result.Substring(posStart + 2);
                        int length = s.IndexOf(";") - 1;
                        resultString = s.Substring(1, length);
                    }
                }
            }
            return (resultString.Length > 0) ? true : false;
        }

        #endregion

        #region InsertWeight

        public static int InsertWeight(int customerId, int articleId, double weight, double netWeight, double tareWeigh, string unit, int transportId, int vehicleId)
        {
            string result = "";

            if (Connect())
            {
                // '?' represents a value to insert
                client.Write("#XConsorzioExtension.InsertTransaction????????");
                client.Write(string.Chr(8));
                client.Write(customerId.ToString());
                client.Write(string.Chr(8));
                client.Write(articleId.ToString());
                client.Write(string.Chr(8));
                client.Write(weight.ToString());
                client.Write(string.Chr(8));
                client.Write(netWeight.ToString());
                client.Write(string.Chr(8));
                client.Write(tareWeigh.ToString());
                client.Write(string.Chr(8));
                client.Write(unit);
                client.Write(string.Chr(8));
                client.Write(transportId.ToString());
                client.Write(string.Chr(8));
                client.Write(vehicleId.ToString());

                client.Write("\f");


                double tEnd = DeviceTimer.GetTicks() + 2 /* sec */;
                bool quit = false;
                do
                {
                    result += client.Read(50, 50);
                    if (result.IndexOf("\f") > 1)
                        quit = true;
                }
                while (!quit && tEnd > DeviceTimer.GetTicks());

                if (quit)
                {
                    // receive ok
                    TcpClient.Close();
                    int pos = result.IndexOf("#l");
                    if (pos > 0 && result.Length > 2)
                    {
                        int FormFeed = result.IndexOf("\f");
                        return int.Parse(result.Substring(pos + 2, FormFeed - pos - 2));
                    }
                }
                else
                {
                    return 0;
                }
            }
            return 0;
        }

        #endregion

        #region UpdateTransaction

        public static int UpdateTransaction(int transactionId, double weight, double netWeight, double tareWeigh, string unit)
        {
            string result = "";

            if (Connect())
            {
                //'?' represents a value to insert
                client.Write("#XConsorzioExtension.UpdateTransaction?????");
                client.Write(string.Chr(8));
                client.Write(transactionId.ToString());
                client.Write(string.Chr(8));
                client.Write(weight.ToString());
                client.Write(string.Chr(8));
                client.Write(netWeight.ToString());
                client.Write(string.Chr(8));
                client.Write(tareWeigh.ToString());
                client.Write(string.Chr(8));
                client.Write(unit);
                client.Write("\f");


                double tEnd = DeviceTimer.GetTicks() + 2 /* sec */;
                bool quit = false;
                do
                {
                    result += client.Read(50, 50);
                    if (result.IndexOf("\f") > 1)
                        quit = true;
                }
                while (!quit && tEnd > DeviceTimer.GetTicks());

                if (quit)
                {
                    // receive ok
                    TcpClient.Close();
                    int pos = result.IndexOf("#l");
                    if (pos > 0 && result.Length > 2)
                    {
                        int FormFeed = result.IndexOf("\f");
                        return int.Parse(result.Substring(pos + 2, FormFeed - pos - 2));
                    }
                }
                else
                {
                    return 0;
                }
            }
            return 0;
        }

        #endregion
    }
}
