﻿using mt.consorzio.Configuration;
using System;
using System.Terminal;

namespace mt.consorzio.Logic
{
    public static class SplashScreen
    {
        private static readonly Label label1 = new Label(1, 26, 16, Program.customer, Fonts.Alpha16x08, 1);
        /// <summary>
        /// Stores the label2 control
        /// </summary>					
        private static readonly Label label2 = new Label(2, 95, 33, Program.version, Fonts.Alpha12x06, 1);
        /// <summary>
        /// Stores the label3 control
        /// </summary>					
        private static readonly Label label3 = new Label(3, 24, 47, "2023 Mettler Toledo (Schweiz) GmbH", Fonts.Alpha12x06, 1);
        public static void ShowDialog(string productName, string version)
        {
            double timeOut;
            Display.SetupWeightDisplay(WeightDisplayVisibility.Off);
            Display.Clear();

            label1.Draw();
            label2.Draw();
            label3.Draw();
            Display.DisplayPage(1);

            SoftKeys.Clear();
            SoftKeys.Define(Keys.SoftKey5, "\\exitapp.bmp");
            SoftKeys.Replace();
            
            timeOut = 3.0 + DeviceTimer.GetTicks();
            do
            {
                if (Program.Key == KeyCodes.Softkey05)
                {
                    Environment.Exit();
                }
            } while (timeOut > DeviceTimer.GetTicks());
        }
    }
}
