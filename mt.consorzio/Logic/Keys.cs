﻿using System;
using System.Terminal;

namespace mt.consorzio.Logic
{
    public enum KeyCodes
    {
        None = 0,
        Softkey01 = 100,
        Softkey02,
        Softkey03,
        Softkey04,
        Softkey05,
        Softkey06,
        Softkey07,
        Softkey08,
        Softkey09,
        Softkey10,
        Softkey11,
        Softkey12,
        Softkey13,
        Softkey14,
        Softkey15
    }
}
