﻿using System;
using System.Terminal;

namespace mt.consorzio.Configuration
{
    public static class Settings
    {
        //--------------------------------------------------
        [Share(SD.Application.SetupString, 10)]
        private static string _password;

        public static string Password
        {
            [Inline]
            get
            {
                return _password;
            }
            [Inline]
            set
            {
                _password = value;
            }
        }

        //--------------------------------------------------
        [Share(SD.Application.SetupInteger, 10)]
        private static int _hostPort;

        public static int HostPort
        {
            [Inline]
            get
            {
                return _hostPort;
            }
            [Inline]
            set
            {
                _hostPort = value;
            }
        }

        //--------------------------------------------------
        [Share(SD.Application.SetupString, 11)]
        private static string _hostIp;

        public static string HostIpAddress
        {
            [Inline]
            get
            {
                return _hostIp;
            }
            [Inline]
            set
            {
                _hostIp = value;
            }
        }

       


        public static void InitOnce()
        {
            Display.Clear();
            Display.SetupWeightDisplay(WeightDisplayVisibility.Off);
            Display.DrawCentered(1, 20, "Init Data");
            Display.DrawCentered(2, 28, "at first run");
            Display.DrawCentered(3, 40, "V1.0.0");
            Password = "1111";
            HostIpAddress = "192.168.174.32";
            HostPort = 20000;
            Program.DPIFactor = 12;

            // TODO create settings screen
            // TODO remove hard coded text
            //--------------------------------------------------
        }
    }
}
