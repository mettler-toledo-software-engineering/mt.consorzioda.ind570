<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
 
<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
 
<xsl:template match="/report">
	<html>
		<head>
			<title>Compiler Report</title>
			<style type="text/css">
				<xsl:text><![CDATA[
					body {
						font-family: Calibri, Verdana, sans-serif;
						font-size: 12px;
						margin: 0;
						padding: 0;
					}
					
					h1 {
						border-top: 1px solid black;
						border-bottom: 1px solid black;
						background-color: #EEE;
						line-height: 60px;
						margin: 10px 0 10px 0;
						padding-left: 20px;
						font-size: 30px;
					}

					h2 {
						border-top: 1px solid black;
						border-bottom: 1px solid black;
						background-color: #DDD;
						line-height: 30px;
						margin: 10px 0 10px 0;
						padding-left: 20px;
						font-size: 20px;
					}
					
					th {
						text-align: left;
						padding: 1px 5px 1px 5px;
					}
					
					tr.a-row td {
						background-color: #EEE;	
					}
					
					tr.b-row td {
						background-color: #FFF;
					}
					
					table {
						border-collapse: collapse;
						margin: 0 0 20px 20px;
					}
					
					table tr td, table tr th {
						border-bottom: 1px solid #000;
						border-right: 1px solid #000;
						margin: 0;
						padding: 3px 5px;
					}
					
					a:link, a:visited, a:active {
						color: blue;
						text-decoration: underline;
					}

					a:hover, a:focus {
						color: blue;
						text-decoration: none;
					}
					
					*.block {
						margin: 20px;
					}
					]]></xsl:text>
			</style>
		</head>
		<body>
			<h1>Compiler Report</h1>
			<table>
				<tr><th><xsl:text>Timestamp: </xsl:text></th><td><xsl:value-of select="created"/></td></tr>
				<tr><th><xsl:text>Output: </xsl:text></th><td><xsl:value-of select="output"/></td></tr>
				<tr><th><xsl:text>Processed expressions: </xsl:text></th><td><xsl:value-of select="exprcount"/></td></tr>
				<tr><th><xsl:text>Methods: </xsl:text></th><td><xsl:value-of select="methodcount"/></td></tr>
				<tr><th><xsl:text>Target: </xsl:text></th><td><xsl:value-of select="target"/></td></tr>
				<tr><th><xsl:text>Generated lines: </xsl:text></th><td><xsl:value-of select="lines"/>/<xsl:value-of select="linelimit"/> (<xsl:value-of select="lineperc"/>)</td></tr>
				<tr><th><xsl:text>File size: </xsl:text></th><td><xsl:value-of select="size"/>/<xsl:value-of select="sizelimit"/> (<xsl:value-of select="sizeperc"/>)</td></tr>
				<tr><th><xsl:text>Global variables: </xsl:text></th><td><xsl:value-of select="varcount"/>/<xsl:value-of select="varlimit"/> (<xsl:value-of select="varperc"/>)</td></tr>
				<tr><th><xsl:text>Stack (String): </xsl:text></th><td><xsl:value-of select="stacksize_string"/></td></tr>
				<tr><th><xsl:text>Stack (Integer): </xsl:text></th><td><xsl:value-of select="stacksize_int"/></td></tr>
				<tr><th><xsl:text>Stack (Double): </xsl:text></th><td><xsl:value-of select="stacksize_double"/></td></tr>
				<tr><th><xsl:text>Stack (Single): </xsl:text></th><td><xsl:value-of select="stacksize_single"/></td></tr>
			</table>
			
			<h1>Method Table</h1>
			<table>
				<tr><th>Name:</th><th>Inline:</th><th>.cctor:</th><th>Stack$:</th><th>Stack%:</th><th>Stack#:</th><th>Stack!:</th><th>Callers:</th><th>Callees:</th><th>Refs:</th></tr>
				<xsl:apply-templates select="method">
					<xsl:sort select="@name" />
				</xsl:apply-templates>
			</table>

			<h1>Variable Table</h1>
			<table>
				<tr><th>Name:</th><th>Dim:</th><th>Name:</th><th>Share:</th><th>Events:</th><th>Common:</th><th>Type:</th></tr>
				<xsl:apply-templates select="var">
					<xsl:sort select="@field" />
 				</xsl:apply-templates>
			</table>
			
			<xsl:if test="count(methodDetail) &gt; 0">
				<h1>Method Details:</h1>
				<xsl:apply-templates select="methodDetail">
					<xsl:sort select="@name" />
 				</xsl:apply-templates>				
			</xsl:if>
	  
		</body>
	</html>
</xsl:template>
 
<xsl:template match="method">
	<xsl:variable name="css-class">
		<xsl:choose>
			<xsl:when test="position() mod 2 = 0">a-row</xsl:when>
			<xsl:otherwise>b-row</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<tr class="{$css-class}">
		<td><a href="#{@name}"><xsl:value-of select="@name"/></a></td>
		<td><xsl:value-of select="@inlined"/></td>
		<td><xsl:value-of select="@constructor"/></td>
		<td><xsl:value-of select="@stack_string"/></td>
		<td><xsl:value-of select="@stack_int"/></td>
		<td><xsl:value-of select="@stack_double"/></td>
		<td><xsl:value-of select="@stack_single"/></td>
		<td><xsl:value-of select="@callercount"/></td>
		<td><xsl:value-of select="@calleecount"/></td>
		<td><xsl:value-of select="@refcount"/></td>
	</tr>
</xsl:template>

<xsl:template match="methodDetail">
	<a name="{@name}" />
	<h2><xsl:value-of select="@name"/></h2>
	<div class="block">
		<xsl:if test="count(callee) &gt; 0">
		<b>This method calls:</b>
		<ol>
			<xsl:apply-templates select="callee"/>		
		</ol>
		</xsl:if>

		<xsl:if test="count(caller) &gt; 0">
		<b>This method gets called from:</b>
		<ol>
			<xsl:apply-templates select="caller"/>		
		</ol>
		</xsl:if>

		<xsl:if test="count(referenceHolder) &gt; 0">
		<b>This method gets referenced from:</b>
		<ol>
			<xsl:apply-templates select="referenceHolder"/>		
		</ol>
		</xsl:if>
	</div>
</xsl:template>

<xsl:template match="caller">
	<li><a href="#{.}"><xsl:value-of select="."/></a></li>
</xsl:template>

<xsl:template match="callee">
	<li><a href="#{.}"><xsl:value-of select="."/></a></li>
</xsl:template>

<xsl:template match="referenceHolder">
	<li><a href="#{.}"><xsl:value-of select="."/></a></li>
</xsl:template>

<xsl:template match="var">
	<xsl:variable name="css-class">
		<xsl:choose>
			<xsl:when test="position() mod 2 = 0">a-row</xsl:when>
			<xsl:otherwise>b-row</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<tr class="{$css-class}">
		<td><xsl:value-of select="@field"/></td>
		<td><xsl:value-of select="@dimensions"/></td>
		<td><xsl:value-of select="@name"/></td>
		<td><xsl:value-of select="@share"/></td>
		<td><xsl:value-of select="@events"/></td>
		<td><xsl:value-of select="@common"/></td>
		<td><xsl:value-of select="@datatype"/></td>
	</tr>
</xsl:template>
 
</xsl:stylesheet>
