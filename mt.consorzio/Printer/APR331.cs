﻿using System;
using System.Terminal;
using mt.consorzio.Configuration;

namespace mt.consorzio.Printer
{
    public static class APR331
    {
        private const int MaxLine = 48;
        public static readonly SerialPort _serialClient = new SerialPort(2, 1, AccessType.Write, 300, '\r', 500, SerialPortFlags.AutoFlush);

        const string cut = "\x1b" + "C1";
        const string wide = "\x0e";
        const string endwide = "\x0f";
        private static bool fileExists = true;
        
           

        #region WriteSerialLine - Open serial port
        public static bool FileExists()
        {
                        return fileExists;
        }

       
        public static void RePrint()
        {
            string str;
            File file = new File(3);
            file.Open("templ.txt", FileAccess.Input);
            while (!file.AtEndOfStream)
            {
                file.ReadLine(out str);
                _serialClient.WriteLine(str);
               // Task.Sleep(100);
            }
            file.Close();
        }

        public static void InitPrint()
        {
            File.Delete("templ.txt");
            fileExists = false;
        }

        static void WriteSerialLine(string str)
        {
            File file = new File(3);
            file.Open("templ.txt", FileAccess.Append);
            file.WriteLine(str);
            file.Close();
            fileExists = true;
            _serialClient.WriteLine(str); // (str  + "\n") = mit einem Umbruch pro Zeile
        //    Task.Sleep(100);
        }
       

        #endregion

        #region WriteLine formatting methods

        private static void WriteCentralLine(string textC)
        {
            int padding = (MaxLine - textC.Length) / 2;
            string filling = "".PadRight(padding, " ");
            string line = filling + textC + filling;

            WriteSerialLine(line);
        }

        private static void WriteLeftRightLine(string textL, string textR)
        {
            int padding = MaxLine - textL.Length - textR.Length;
            string line = textL + "".PadRight(padding, " ") + textR;

            WriteSerialLine(line);
        }

        private static void PrintCharLine(string sChar)
        {
            string line = "".PadRight(MaxLine, sChar);

            WriteSerialLine(line);
        }

        #endregion

        public static void WriteLabel()
        {
            WriteTitle();
            WriteHeader();
            WriteBody();
            WriteFooter();
        }

        private static void WriteTitle()
        {
            PrintCharLine("*");
            WriteCentralLine(wide + "CDALED" + endwide);
            WriteCentralLine("Consorzio Depurazione Acque");
            WriteCentralLine("Lugano e Dintorni");
            PrintCharLine("*");
        }

        private static void WriteHeader()
        {
            WriteSerialLine(" ");
            WriteLeftRightLine("Bolletino No: ", Program.BolletionNr.ToString());
            WriteLeftRightLine("Data / ora: ", Program.TimeStamp);
            WriteSerialLine(" ");
            WriteLeftRightLine("Transportatore: ", Program.Transporter);
            WriteLeftRightLine("Veicolo: ", Program.Vehicle);
            WriteLeftRightLine("Cliente: ", Program.Customer);
            WriteLeftRightLine("Prodotto: ", Program.Article);
        }

        private static void WriteBody()
        {
            string unit = " " + Program.Unit;
            string pesata1 = Program.FirstWeight.ToString() + unit;
            string pesata2 = Program.SecondWeight.ToString() + unit;
            double nettoDouble = Program.FirstWeight - Program.SecondWeight;
            string netto = nettoDouble.ToString() + unit;

            WriteSerialLine(" ");
            WriteLeftRightLine("1. Pesata: ", pesata1);
            WriteLeftRightLine("2. Pesata: ", pesata2);
            WriteSerialLine(" ");
            WriteLeftRightLine("Netto: ", netto);
            WriteSerialLine(" ");
            WriteSerialLine(" ");
            WriteSerialLine(" ");
        }

        private static void WriteFooter()
        {
            WriteSerialLine(" ");
            WriteSerialLine(" ");
            WriteSerialLine(" ");
            WriteSerialLine(" ");
            WriteSerialLine(" ");
            WriteSerialLine(cut);

        }
    }
}
