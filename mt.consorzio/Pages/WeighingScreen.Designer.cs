// This code is autogenerated by the Runtime Screen Designer
// Do not modify this code, your changes will be lost when using the designer view

using System;
using System.Terminal;

namespace mt.consorzio.Pages
{
	public static partial class WeighingScreen
	{
		/// <summary>
		/// Internal. Stores if the dialog should be closed after the next event
		/// </summary>	
		private static bool _closing = false;
	
		/// <summary>
		/// Internal. Stores if the dialog should be repainted after the next event
		/// </summary>			
		private static bool _redraw = false;
		/// <summary>
		/// Stores the sharedDataLabel1 control
		/// </summary>					
		private static readonly SharedDataLabel<string> sharedDataLabel1 = new SharedDataLabel<string>(1, 64, 10, "AK0107", "!0.0", Fonts.Alpha16x08, 1);
							
		/// <summary>
		/// Displays the dialog
		/// </summary>
		public static void ShowDialog()
		{
			_closing = false;
			_redraw = true;
			Initialize();
			do
			{			
				if (_redraw)
				{
					BeforePaint();
					InitializeComponents();
					AfterPaint();
					_redraw = false;
				}			
				
				Input.DoEvents();
				EventOccured();
				
				switch(RuntimeScreen.ReadKey())
				{
					case 100:
						cmdExit_Triggered();
						break;
					case 104:
						cmdOk_Triggered();
						break;
				
				}
			} while (!_closing);
			Closed();
		}
		
		private static void InitializeComponents()
		{
			Display.Clear();
			Display.SetupWeightDisplay(WeightDisplayVisibility.On, WeightDisplaySize.Medium);

		    sharedDataLabel1.Draw();
			Display.DisplayPage(1);

		
			// Update softkeys
			SoftKeys.Clear();
			SoftKeys.Define(Keys.SoftKey1, 100, "\\exit.bmp");
			SoftKeys.Define(Keys.SoftKey5, 104, "\\ok.bmp");
							
			DefineSoftkeys();
			SoftKeys.Replace();
		}
		
		/// <summary>
		/// Closes the dialog
		/// </summary>
		[Inline]
		public static void Close()
		{
			_closing = true;
		}
		
		/// <summary>
		/// Forces a redraw of the runtime screen
		/// </summary>
		[Inline]
		private static void Redraw()
		{
			_redraw = true;
		}
		
		/// <summary>
		/// Occures when the dialog is initialized
		/// </summary>
		static partial void Initialize();		

		/// <summary>
		/// Occures before the dialog is repainted
		/// </summary>
		static partial void BeforePaint();

		/// <summary>
		/// Occures when the softkeys are being defined
		/// </summary>
		static partial void DefineSoftkeys();

		/// <summary>
		/// Occures after the dialog is repainted
		/// </summary>
		static partial void AfterPaint();
		
		/// <summary>
		/// Occures after an event
		/// </summary>
		static partial void EventOccured();

		/// <summary>
		/// Occures when the dialog has closed
		/// </summary>
		static partial void Closed();

		/// <summary>
		/// Occures when the user presses the cmdExit key
		/// </summary>	
		static partial void cmdExit_Triggered();

		/// <summary>
		/// Occures when the user presses the cmdOk key
		/// </summary>	
		static partial void cmdOk_Triggered();
	}
}



