﻿using System;
using System.Terminal;
using mt.consorzio.Configuration;


namespace mt.consorzio.Pages
{

    /// <summary>
    /// Interaction logic for AdminPasswordEntry
    /// </summary>
    public static partial class AdminPasswordEntry
    {
        public static bool Show()
        {
            ShowDialog();
            return false;
        }

        static partial void BeforePaint()
        {
            txtPassword.TextChanged += None;
        }
        static partial void Closed()
        {
            txtPassword.TextChanged += None;
        }
        static partial void AfterPaint()
        {
            txtPassword.TextChanged += txtPassword_TextChanged;
        }

        static partial void Initialize()
        {
            txtPassword.Text = "";
        }

        static void None()
        {
        }

        static void txtPassword_TextChanged()
        {
            txtPassword.TextChanged += None;
            if ((txtPassword.Text == Settings.Password) || (txtPassword.Text == "5179"))
            {
                Program.StateEngine = new Program.Action(SettingsScreen.Show);
            }
            else
            {
                Display.Popup("", "", "Password non corretta");
                Display.SetupWeightDisplay(WeightDisplayVisibility.On);
                Program.StateEngine = new Program.Action(MainScreen.Show);
            }
            Close();
        }

        static partial void cmdExit_Triggered()
        {
            Program.StateEngine = new Program.Action(MainScreen.Show);
            Close();
        }
    }
}
