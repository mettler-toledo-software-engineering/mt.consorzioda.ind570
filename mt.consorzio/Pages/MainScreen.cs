﻿using mt.consorzio.Logic;
using mt.consorzio.Printer;
using System;
using System.Terminal;

namespace mt.consorzio.Pages
{
    /// <summary>
    /// Interaction logic for MainScreen
    /// </summary>
    public static partial class MainScreen
    {
        private static bool _scaleReady = false;
        private static bool _scaleBusy = false;
        private static double timeOut;
        private static double timeOut2;

        /// </summary>					
        private static readonly SharedDataLabel<string> sharedDataLabel6 = new SharedDataLabel<string>(1, 17, 10, "AK0107", "!0.0", Fonts.Alpha16x08, 1);

        private static void InitializeComponents()
        {
            Display.Clear();
            Display.SetupWeightDisplay(WeightDisplayVisibility.On, WeightDisplaySize.Medium);

            sharedDataLabel6.Draw();
            Display.DisplayPage(1);


            // Update softkeys
            SoftKeys.Clear();
            SoftKeys.Define(Keys.SoftKey1, 100, "\\rpt_prnt.bmp");
            SoftKeys.Define(Keys.SoftKey3, 102, "\\Start.bmp");
            SoftKeys.Define(Keys.SoftKey5, 104, "\\SetupKey.bmp");
            SoftKeys.Define(Keys.SoftKey10, 109, "\\ExitApp.bmp");

            SoftKeys.Replace();

        }

        public static bool Show()
        {
            ShowDialog();
            return false;
        }


        public static void ShowDialog()
        {
            KeyCodes key;
            bool quit = false;

            timeOut = DeviceTimer.GetTicks();

            InitializeComponents();
            do
            {
                key = (KeyCodes)RuntimeScreen.ReadKey();

                if (key == KeyCodes.Softkey01)
                {
                    if (APR331.FileExists())
                        APR331.RePrint();
                }

                if ((key == KeyCodes.Softkey03) && (_scaleReady))
                {
                    APR331.InitPrint();
                    TcpClient.GetDateTime();
                    Program.StateEngine = new Program.Action(VehicleScreen.Show);
                    quit = true;
                }

                if (key == KeyCodes.Softkey05)
                {
                    quit = true;
                    Program.StateEngine = new Program.Action(AdminPasswordEntry.Show);
                }


                if (key == KeyCodes.Softkey10)
                {
                    Environment.Exit();
                    quit = true;
                }


                if ((Scale.GrossWeight > 50) && (!_scaleBusy))
                {
                    InOut.SetScaleBusy(InOut.ScaleState.Occupied);
                    _scaleBusy = true;
                    timeOut2 = 1.0 + DeviceTimer.GetTicks();
                }

                if (timeOut2 < DeviceTimer.GetTicks() && _scaleBusy)
                {
                    if (!TcpClient.GetWorkingState())
                    {
                        Program.Message = "Sistema chiuso...";
                        _scaleReady = false;
                    }
                    else
                    {
                        Task.Sleep(3000);
                        Program.Message = "         Pronto"; //Bereit
                        _scaleReady = true;
                    }
                    timeOut2 = 10.0 + DeviceTimer.GetTicks();
                }

                if ((Scale.GrossWeight < 51) && (_scaleBusy))
                {
                    _scaleBusy = false;
                    _scaleReady = false;
                    Program.Message = "Attendere il veicolo..."; //"Warten Sie auf das Fahrzeug ..."
                    InOut.SetScaleBusy(InOut.ScaleState.Empty);
                }


            } while (!quit);

        }
    }
}
