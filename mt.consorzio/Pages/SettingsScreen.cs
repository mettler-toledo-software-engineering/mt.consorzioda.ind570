﻿using mt.consorzio.Configuration;
using System;
using System.Terminal;

namespace mt.consorzio.Pages
{
    /// <summary>
    /// Interaction logic for SettingsScreen
    /// </summary>
    public static partial class SettingsScreen
    {
        static partial void Initialize()
        {
            LoadEntries();
        }

        public static bool Show()
        {
            ShowDialog();
            return false;
        }

        #region LoadEntries

        static void LoadEntries()
        {
            txtHostIPAddress.Text = Settings.HostIpAddress;
            txtHostPort.Text = Settings.HostPort.ToString();
            txtPassword.Text = Settings.Password;
        }

        #endregion

        #region SaveEntries

        static void SaveEntries()
        {
            Settings.HostIpAddress = txtHostIPAddress.Text;
            Settings.HostPort = int.Parse(txtHostPort.Text);
            Settings.Password = txtPassword.Text;
        }

        #endregion

        static partial void cmdBack_Triggered()
        {
            ReturnToMain();
        }

        static partial void cmdOk_Triggered()
        {
            SaveEntries();
            ReturnToMain();
        }

        static void ReturnToMain()
        {
            Program.StateEngine = new Program.Action(MainScreen.Show);
            Close();
        }
    }
}
