﻿using System;
using System.Terminal;
using mt.consorzio.Logic;
using mt.consorzio.Printer;

namespace mt.consorzio.Pages
{
    /// <summary>
    /// Interaction logic for WeighingScreen
    /// </summary>
    public static partial class WeighingScreen
    {

        static partial void Initialize()
        {
            Program.Message = "Pesatura con OK.."; //"Wiegen mit OK .."
        }

        public static bool Show()
        {
            ShowDialog();
            return false;
        }

        static partial void cmdExit_Triggered()
        {
            Program.StateEngine = new Program.Action(MainScreen.Show);
            Close();
        }

        static partial void cmdOk_Triggered()
        {
            int result = -1;

            if (!Scale.InMotion)
            {
                Scale.AlibiPrint();
                double grossWeight = Scale.GrossWeight;
                double tareWeight = Scale.TareWeight;
                double netWeight = grossWeight - tareWeight;
                Program.Unit = Scale.Unit;

                TcpClient.GetTransactionData(Program.VehicleId);
                int transactionId = Program.TransactionId;

                // TODO if there is no transaction: insert new transaction with first weight
                if (transactionId == 0)
                {
                    result = TcpClient.InsertWeight(Program.CustomerId, Program.ArticleId, grossWeight, netWeight, tareWeight, Program.Unit, Program.TransportId, Program.VehicleId);

                    if (result > 0)
                    {
                        Display.Popup("Messaggio", "Pesatura registrata,", "si prega di allontanarsi!"); //„Nachricht“, „Wiegen erfasst“, „Bitte weggehen!“
                        ResetSignalAndReturn();
                    }
                    else
                        Display.Popup("Errore", "Inserire fallito!", "Pesatura non registrata"); //"Fehler","Eingabe fehlgeschlagen!", "Wiegen nicht erfasst"
                }
                else
                    Display.Popup("Errore", "Pesatura iniziale", "già disponibile"); //"Fehler", "Erstwägung schon vorhanden"
            }
            else
                Display.Popup("Messaggio", "Bilance in movimento!"); //"Nachricht", "Waage in Bewegung!"
        }

        private static void ResetSignalAndReturn()
        {
            Program.Message = "Si prega di allontanarsi"; //"Bitte geh weg"
            InOut.SetScaleBusy(InOut.ScaleState.ReadyToLeave);
            Program.StateEngine = new Program.Action(MainScreen.Show);
            Close();
        }
    }
}
