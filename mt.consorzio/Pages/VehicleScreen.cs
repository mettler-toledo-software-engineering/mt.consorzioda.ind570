﻿using mt.consorzio.Logic;
using mt.consorzio.Printer;
using System;
using System.Terminal;

namespace mt.consorzio.Pages
{
    /// <summary>
    /// Interaction logic for VehicleScreen
    /// </summary>
    public static partial class VehicleScreen
    {
        static partial void Initialize()
        {
            txtVehicleCode.Text = "";
        }

        public static bool Show()
        {
            ShowDialog();
            return false;
        }

        static partial void BeforePaint()
        {
            txtVehicleCode.TextChanged += None;
        }

        static partial void Closed()
        {
            txtVehicleCode.TextChanged += None;
        }

        static partial void AfterPaint()
        {
            txtVehicleCode.TextChanged += txtCompanyCode_TextChanged;
        }

        static void None()
        {
        }

        static partial void cmdExit_Triggered()
        {
            Program.StateEngine = new Program.Action(MainScreen.Show);
            Close();
        }

        static void txtCompanyCode_TextChanged()
        {
            
            // VEICOLO
            if (txtVehicleCode.Text != "")
            {
                TcpClient.GetVehicleData(txtVehicleCode.Text.TrimStart().TrimEnd());

                if (Program.TransportId > 0)   // Auto und Firma gefunden
                {
                    TcpClient.GetTransactionData(Program.VehicleId);  // offene Transaktion ?
                    if (Program.TransactionId == 0)   // Erstwägung
                    {
                        TcpClient.GetTransportData(Program.TransportId);

                        if (Program.Transporter != "")
                        {
                            txtVehicleCode.TextChanged += None;
                            Program.StateEngine = new Program.Action(ArticleScreen.Show);
                            Close();
                        }
                        else
                            Display.Popup("Società", "Non trovato!");
                    }
                    else   // direkt Zweitwägung
                    {
                        if (!Scale.InMotion)
                        {
                            Scale.AlibiPrint();
                            int result = -1;
                            double grossWeight = Scale.GrossWeight;
                            double tareWeight = Scale.TareWeight;
                            double netWeight = grossWeight - tareWeight;
                            Program.Unit = Scale.Unit;

                            Program.SecondWeight = netWeight;
                            result = TcpClient.UpdateTransaction(Program.TransactionId, grossWeight, netWeight, tareWeight, Program.Unit);
                            TcpClient.GetArticleData("", Program.ArticleId.ToString());
                            TcpClient.GetCustomerData("", Program.CustomerId.ToString());
                            TcpClient.GetTransportData(Program.TransportId);
                            TcpClient.GetFirstWeighingData(Program.FirstWeightId.ToString());

                            if (result > 0)
                            {
                                Program.TimeStamp = DateTime.Now.ToString();
                                Program.BolletionNr = Program.TransactionId;
                                APR331.WriteLabel();
                                Program.ResetLabels();
                                Program.Message = "Registrato, allontanati!"; //"Bitte geh weg"
                                InOut.SetScaleBusy(InOut.ScaleState.ReadyToLeave);
                                //Display.Popup("Messaggio", "Pesatura registrata,", "si prega di allontanarsi!"); //„Nachricht“, „Wiegen erfasst“, „Bitte weggehen!“
                            }
                            else
                                Display.Popup("Aggiornamento", "Fallito!"); //"Update fehlgeschlagen!"
                           
                            txtVehicleCode.TextChanged += None;
                            Program.StateEngine = new Program.Action(MainScreen.Show);
                            Close();
                        }
                        else
                            Display.Popup("Messaggio", "Bilance in movimento!"); //"Nachricht", "Waage in Bewegung!"
                    }
                }
                else
                {
                    Display.Popup("Veicolo", "Non trovato!");
                }
            }

        }
    }
}
