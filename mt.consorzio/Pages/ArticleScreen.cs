﻿using System;
using System.Terminal;
using mt.consorzio.Pages;
using mt.consorzio.Logic;

namespace mt.consorzio.Pages
{
    /// <summary>
    /// Interaction logic for ArticleScreen
    /// </summary>
    public static partial class ArticleScreen
    {
        static partial void Initialize()
        {
            txtArticleCode.Text = "";
        }

        public static bool Show()
        {
            ShowDialog();
            return false;
        }


        static partial void BeforePaint()
        {
            txtArticleCode.TextChanged += None;
        }
        static partial void Closed()
        {
            txtArticleCode.TextChanged += None;
        }
        static partial void AfterPaint()
        {
            txtArticleCode.TextChanged += txtArticleCode_TextChanged;
        }

        static void None()
        {
        }

        static partial void cmdExit_Triggered()
        {
            Program.StateEngine = new Program.Action(VehicleScreen.Show);
            Close();
        }


        static void txtArticleCode_TextChanged()
        {
            // ARTICOLO

            if (txtArticleCode.Text != "")
            {
                TcpClient.GetArticleData(txtArticleCode.Text,"");

                if (Program.Article != "")
                {
                    Program.StateEngine = new Program.Action(CustomerScreen.Show);
                    Close();
                }
                else
                {
                    // not found
                    Display.Popup("Articolo", "Non trovato!");
                }
            }

        }
    }
}
