﻿using System;
using System.Terminal;
using mt.consorzio.Logic;
using mt.consorzio.Pages;
using mt.consorzio.Configuration;

namespace mt.consorzio
{
    static class Program
    {
        public const string manufacturer = "©2022 Mettler Toledo (Schweiz) GmbH";
        public const string version = "Version 1.0.4";
        public const string customer = "Consorzio Depurazione Acque";
        public const string projectnr = "Project P21071301";
        private const string programTitle = customer + projectnr + version;

        [Share(SD.System.Setup, 1, 10)]
        public static int DateFormat;
        [Share(SD.System.Setup, 1, 11)]
        public static int TimeFormat;
        [Share(SD.System.Setup, 1, 12)]
        public static string DateSeparator;
        [Share(SD.System.Setup, 1, 13)]
        public static string TimeSeparator;

        [Share(SD.Application.SetupString, 2)]
        public static string FirstRun;

        [Share(SD.Application.DynamicString, 1)]
        public static string Transporter;
        [Share(SD.Application.DynamicString, 2)]
        public static string Vehicle;
        [Share(SD.Application.DynamicString, 3)]
        public static string TimeStamp;
        [Share(SD.Application.DynamicString, 4)]
        public static string Article;
        [Share(SD.Application.DynamicString, 5)]
        public static string Customer;
        [Share(SD.Application.DynamicString, 6)]
        public static string Unit;

        [Share(SD.Application.DynamicString, 7)]
        public static string Message;

        [Share(SD.Application.DynamicString, 8)]
        public static string CustomerCode;
        [Share(SD.Application.DynamicString, 9)]
        public static string ArticleCode;

        [Share(SD.Application.DynamicInteger, 1)]
        public static int ClientNr;
        [Share(SD.Application.DynamicInteger, 2)]
        public static int BolletionNr;
        [Share(SD.Application.DynamicInteger, 3, FireEvents = true)]
        public static int CommandTrigger;

        [Share(SD.System.Commands, 1, 12)]
        public static string CurrrentDate;
        [Share(SD.System.Commands, 1, 11)]
        public static string CurrrentTime;

        public static int CustomerId;
        public static int TransportId;
        public static int VehicleId;
        public static int ArticleId;
        public static int TransactionId;
        public static int FirstWeightId;

        [Share(SD.Application.DynamicDecimal, 1)]
        public static double FirstWeight;
        [Share(SD.Application.DynamicDecimal, 2)]
        public static double SecondWeight;

        public static KeyCodes Key;
        public static double DPIFactor;

        public delegate bool Action();
        public static Action StateEngine;

        /// <summary>
        /// Main entry point of your application
        /// </summary>
        static void Main()
        {
            TimeFormat = 3;
            DateFormat = 7;
            DateSeparator = ".";
            TimeSeparator = ":";

            if (FirstRun != programTitle)
            {
                FirstRun = programTitle;
                //Settings.InitOnce();  Einstellungen von Consorzio werden übernommen
            }
            Scale.Initialize();
            SplashScreen.ShowDialog(customer, version);
            RuntimeScreen.Initialize();
            ResetLabels();

            StateEngine = new Action(MainScreen.Show);

            while (!StateEngine()) ;
        }

        public static void ResetLabels()
        {
            Transporter = "";
            Vehicle = "";
            TimeStamp = "";
            Article = "";
            Customer = "";
            Unit = "";

            ArticleCode = "";
            CustomerCode = "";

            CustomerId = 0;
            ArticleId = 0;
            TransactionId = 0;

            ClientNr = 0;
            BolletionNr = 0;

            FirstWeight = 0;
            SecondWeight = 0;
        }
    }
}
