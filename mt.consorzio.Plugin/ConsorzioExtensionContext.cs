﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MT.DataServices.Logging;
using MT.DataServices.Plugins.UnibaseEx.Communication;
using MT.DataServices.Plugins.UnibaseEx.Backends;

namespace mt.consorzio.Plugin
{
    internal class ConsorzioExtensionContext
    {
        private readonly IEndPointHostingService hostingService;
        private readonly ILogSink log;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConsorzioExtensionContext"/> class.
        /// </summary>
        public ConsorzioExtensionContext(IEndPointHostingService hostingService)
        {
            this.hostingService = hostingService;
            this.log = hostingService.Log;
        }

        [ExtensionContract]
        #region InsertTransaction

        public int InsertTransaction(int customerId, int articleId, double grossWeight, double netWeight, double tareWeight, string unit, int transportId, int vehicleId)
        {
            int result = -1;

            string today = DateTime.Now.ToString("yyyy-MM-dd HH:ss:mm");

            int weighingId = InsertWeighingInDB(today, grossWeight, netWeight, tareWeight, unit);
            //int weighingId = GetLastWeighingFromDB();

            if (weighingId > 0)
            {
                result = InsertTransactionInDB(customerId, articleId, weighingId, today, transportId, vehicleId);
            }

            return result;
        }
        #endregion

        #region Helper Methods

        #region InsertWeighingInDB

        private int InsertWeighingInDB(string timeStamp, double grossWeight, double netWeight, double tareWeight, string unit)
        {
            int result = -1;
            using (var backend = hostingService.OpenBackendConnection())
            {
                try
                {
                    backend.Open();

                    // Example: INSERT INTO Weighings (TimeStamp,GrossWeight,NetWeight,TareWeight,WeighUnit) VALUES ('2021-11-18 09:30:44.0164549',123,0,0,'kg')

                    string command = String.Format("INSERT INTO Weighings (TimeStamp,GrossWeight,NetWeight,TareWeight,WeighUnit) VALUES ('{0}', {1}, {2}, {3}, '{4}')",
                        timeStamp, grossWeight, netWeight, tareWeight, unit);

                    log.Hint("execute command: " + command);

                    ParameterValue p1 = new ParameterValue("@p1", timeStamp);
                    ParameterValue p2 = new ParameterValue("@p2", grossWeight);
                    ParameterValue p3 = new ParameterValue("@p3", netWeight);
                    ParameterValue p4 = new ParameterValue("@p4", tareWeight);
                    ParameterValue p5 = new ParameterValue("@p5", unit);

                    backend.ExecuteQuery("INSERT INTO Weighings (TimeStamp,GrossWeight,NetWeight,TareWeight,WeighUnit) VALUES (@p1, @p2, @p3, @p4, @p5)",
                        p1, p2, p3, p4, p5).Dispose();

                    // result = Convert.ToInt32(backend.ExecuteQuery("Select Max(Id) from Weighings").ToScalar());
                    result = Convert.ToInt32(backend.ExecuteQuery("SELECT IDENT_CURRENT('Weighings')").ToScalar());
                }
                finally
                {
                    backend.Close();
                }
            }
            return result;
        }

        #endregion

        #region InsertTransactionInDB

        private int InsertTransactionInDB(int customerId, int articleId, int weighingId, string timeStamp, int transportId, int vehicleId)
        {
            int result = -1;
            using (var backend = hostingService.OpenBackendConnection())
            {
                try
                {
                    backend.Open();

                    ParameterValue p1 = new ParameterValue("@p1", customerId);
                    ParameterValue p2 = new ParameterValue("@p2", articleId);
                    ParameterValue p3 = new ParameterValue("@p3", weighingId);
                    ParameterValue p4 = new ParameterValue("@p4", 1);
                    ParameterValue p5 = new ParameterValue("@p5", 1);
                    ParameterValue p6 = new ParameterValue("@p6", timeStamp);
                    ParameterValue p7 = new ParameterValue("@p7", 0);
                    ParameterValue p8 = new ParameterValue("@p8", transportId);
                    ParameterValue p9 = new ParameterValue("@p9", vehicleId);
                    ParameterValue p10 = new ParameterValue("@p10", 1);

                    // Example: INSERT INTO Transactions (CustomerId,ArticleId,FirstWeighingId,HasBeenPrinted,IsActive,TimeStamp,IsSingleWeighing)
                    //              VALUES(3,3,1015,0,1,'2021-11-18 16:04:00.2527349',3,0)

                    string command = "INSERT INTO Transactions (CustomerId,ArticleId,FirstWeighingId,HasBeenPrinted,IsActive,TimeStamp,IsSingleWeighing,TransportCompanyId, VehicleId, LabelPrinted) ";
                    command += String.Format("VALUES ({0}, {1}, {2}, 1, 1, {3}, 0, {4}, {5}, 1)",
                            customerId, articleId, weighingId, timeStamp, transportId, vehicleId);

                    log.Hint("execute command: " + command);

                    backend.ExecuteQuery("INSERT INTO Transactions (CustomerId,ArticleId,FirstWeighingId,HasBeenPrinted,IsActive,TimeStamp,IsSingleWeighing,TransportCompanyId,VehicleId,LabelPrinted) VALUES (@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10)"
                        , p1, p2, p3, p4, p5, p6, p7, p8, p9, p10).Dispose();
                    result = Convert.ToInt32(backend.ExecuteQuery("SELECT IDENT_CURRENT('Transactions')").ToScalar());
                }
                finally
                {
                    backend.Close();
                }
            }
            return result;
        }

        #endregion
        #endregion

        [ExtensionContract]
        #region UpdateTransaction

        public int UpdateTransaction(int transactionId, double grossWeight, double netWeight, double tareWeight, string unit)
        {
            int result = -1;

            string today = DateTime.Now.ToString("yyyy-MM-dd HH:ss:mm");

            int weighingId = InsertWeighingInDB(today, grossWeight, netWeight, tareWeight, unit);


            if (weighingId > 0)
            {
                UpdateTransactionInDB(weighingId, transactionId);

                result = weighingId;
            }

            return result;
        }

        #region Helper Methods

        #region UpdateTransactionInDB

        private void UpdateTransactionInDB(int weighingId, int transactionId)
        {
            using (var backend = hostingService.OpenBackendConnection())
            {
                try
                {
                    backend.Open();

                    string command = String.Format("UPDATE Transactions SET SecondWeighingId={0} WHERE id={1}",
                        weighingId, transactionId);

                    log.Hint("execute command: " + command);
                    backend.ExecuteQuery(command).Dispose();
                }
                finally
                {
                    backend.Close();
                }
            }
        }

        #endregion

        #endregion

        #endregion

        [ExtensionContract]
        #region GetCustomerData

        public string GetCustomerData(string code, string id)
        {
            string result = "";
            string command = "";

            using (var backend = hostingService.OpenBackendConnection())
            {
                try
                {
                    backend.Open();
                    if (code.Length > 0)
                        command = string.Format("SELECT Id,Name,CustomerCode FROM Customers WHERE CustomerCode = {0}", code);
                    if (id.Length > 0)
                        command = string.Format("SELECT Id,Name,CustomerCode FROM Customers WHERE Id = {0}", id);

                    log.Hint("command: " + command);
                    var activeCustomer = backend.ExecuteQuery(command).ToDataTable();
                    if (activeCustomer.Rows.Count > 0)
                    {
                        int localid = Convert.ToInt32(activeCustomer.Rows[0]["Id"]);
                        string name = activeCustomer.Rows[0]["Name"].ToString();
                        string custCode = activeCustomer.Rows[0]["CustomerCode"].ToString();
                        result = string.Format("{0};{1};{2};", localid, name, custCode);
                        log.Hint(string.Format("GetCustomerData Id {0}, Name {1}, CustomerCode {2}", localid, name, custCode));
                    }
                }
                finally
                {
                    backend.Close();
                }
            }

            return result;
        }

        #endregion

        [ExtensionContract]
        #region GetArticleData

        public string GetArticleData(string code, string id)
        {
            string result = "";
            string command = "";

            using (var backend = hostingService.OpenBackendConnection())
            {
                try
                {
                    backend.Open();
                    if (code.Length > 0)
                        command = string.Format("SELECT Id, ArticleName, ArticleCode FROM Articles WHERE ArticleCode = {0}", code.Trim());
                    if (id.Length > 0)
                        command = string.Format("SELECT Id, ArticleName, ArticleCode FROM Articles WHERE Id = {0}", id.Trim());

                    log.Hint("command: " + command);
                    var activeArticle = backend.ExecuteQuery(command).ToDataTable();
                    if (activeArticle.Rows.Count > 0)
                    {
                        int localid = Convert.ToInt32(activeArticle.Rows[0]["Id"]);
                        string name = activeArticle.Rows[0]["ArticleName"].ToString();
                        string artCode = activeArticle.Rows[0]["ArticleCode"].ToString();
                        result = string.Format("{0};{1};{2};", localid, name, artCode);
                        log.Hint(string.Format("GetArticleData Id {0}, Name {1}, ArticleCode {2}", localid, name, artCode));
                    }
                }
                finally
                {
                    backend.Close();
                }
            }

            return result;
        }
        #endregion

        [ExtensionContract]
        #region GetTransportData

        public string GetTransportData(string code)
        {
            string result = "";

            using (var backend = hostingService.OpenBackendConnection())
            {
                try
                {
                    backend.Open();
                    string command = string.Format("SELECT Id,Name  FROM TransportCompanies WHERE Id = {0}", code);

                    log.Hint("command: " + command);
                    var activeTransport = backend.ExecuteQuery(command).ToDataTable();
                    if (activeTransport.Rows.Count > 0)
                    {
                        int id = Convert.ToInt32(activeTransport.Rows[0]["Id"]);
                        string name = activeTransport.Rows[0]["Name"].ToString();
                        result = string.Format("{0};{1};", id, name);
                        log.Hint(string.Format("GetTransportData Id {0}, Name {1}", id, name));
                    }
                }
                finally
                {
                    backend.Close();
                }
            }

            return result;
        }

        #endregion

        [ExtensionContract]
        #region GetVehicleData

        public string GetVehicleData(string vehicleCode)
        {
            string result = "";

            using (var backend = hostingService.OpenBackendConnection())
            {
                try
                {
                    backend.Open();
                    string command = string.Format("SELECT Id, LicensePlate, TransportCompanyId FROM Vehicles WHERE VehicleCode = {0}", vehicleCode);

                    log.Hint("command: " + command);
                    var activeVehicle = backend.ExecuteQuery(command).ToDataTable();
                    if (activeVehicle.Rows.Count > 0)
                    {
                        int id = Convert.ToInt32(activeVehicle.Rows[0]["Id"]);
                        string licensePlate = activeVehicle.Rows[0]["LicensePlate"].ToString();
                        int transportid = Convert.ToInt32(activeVehicle.Rows[0]["TransportCompanyId"]);
                        result = string.Format("{0};{1};{2};", id, licensePlate, transportid);
                        log.Hint(string.Format("GetVehicleData TransportCompanyId {0}, Id {1}, LicensePlate {2}", transportid, id, licensePlate));
                    }
                }
                finally
                {
                    backend.Close();
                }
            }

            return result;
        }

        #endregion

        [ExtensionContract]
        #region GetFirstWeighingData

        public string GetFirstWeighingData(string weighingId)
        {
            string result = "";

            using (var backend = hostingService.OpenBackendConnection())
            {
                try
                {
                    backend.Open();
                    string command = string.Format("SELECT Id, GrossWeight FROM Weighings WHERE Id = {0}", weighingId);

                    log.Hint("command: " + command);
                    var activeVehicle = backend.ExecuteQuery(command).ToDataTable();
                    if (activeVehicle.Rows.Count > 0)
                    {
                        int id = Convert.ToInt32(activeVehicle.Rows[0]["Id"]);
                        string firstWeight = activeVehicle.Rows[0]["GrossWeight"].ToString();
                        result = string.Format("{0};{1};", id, firstWeight);
                        log.Hint(string.Format("GetFirstWeighingData WeighingId {0}, GrossWeight {1}", id, firstWeight));
                    }
                }
                finally
                {
                    backend.Close();
                }
            }

            return result;
        }

        #endregion

        [ExtensionContract]
        #region GetTransactionData

        public string GetTransactionData(int vehicleId)
        {
            string result = "";

            using (var backend = hostingService.OpenBackendConnection())
            {
                try
                {
                    backend.Open();

                    //Example: SELECT Id FROM [ConsorzioDA].[dbo].[Transactions] Where CustomerId=3 and ArticleId=2 and SecondWeighingId is NULL and IsActive=1

                    string command = "SELECT Id, ArticleId, CustomerID, FirstWeighingId FROM Transactions WHERE VehicleId=" + vehicleId;
                    command += " AND SecondWeighingId is NULL AND IsActive=1";

                    log.Hint("command: " + command);

                    result = Convert.ToString(backend.ExecuteQuery(command).ToScalar());

                    var res = backend.ExecuteQuery(command).ToDataTable();

                    if (res.Rows.Count > 0)
                    {
                        int id = Convert.ToInt32(res.Rows[0]["Id"]);
                        int artId = Convert.ToInt32(res.Rows[0]["ArticleId"]);
                        int custId = Convert.ToInt32(res.Rows[0]["CustomerID"]);
                        int weighid = Convert.ToInt32(res.Rows[0]["FirstWeighingId"]);
                        result = string.Format("{0};{1};{2};{3};", id, artId, custId, weighid);
                        log.Hint(string.Format("GetTransactionData  Id {0}, ArticleId {1}, CustomerID {2}, FirstWeighingId {3}", id, artId, custId, weighid));
                    }
                }
                finally
                {
                    backend.Close();
                }
            }

            return result;
        }

        #endregion

        [ExtensionContract]
        #region GetWorkingState

        public string GetWorkingState()
        {
            string result = "";
            string command = "";

            using (var backend = hostingService.OpenBackendConnection())
            {
                int minOfDay = (int)DateTime.Today.DayOfWeek * 1440 + DateTime.Now.Minute + DateTime.Now.Hour * 60;   //DayOfWeek: So = 0 So = 6
                try
                {
                    backend.Open();

                    command = string.Format("SELECT Id, FromMin, ToMin FROM Working WHERE ToMin >= {0} AND FromMin <= {0}", minOfDay);
                    log.Hint("command: " + command);
                    var activeHour = backend.ExecuteQuery(command).ToDataTable();
                    if (activeHour.Rows.Count > 0)
                    {
                        int id = Convert.ToInt32(activeHour.Rows[0]["Id"]);
                        int fromMin = Convert.ToInt32(activeHour.Rows[0]["FromMin"]);
                        int toMin = Convert.ToInt32(activeHour.Rows[0]["ToMin"]);
                        result = string.Format("{0};{1};{2};", id, fromMin, toMin);
                        log.Hint(string.Format("GetWorkingState Id {0}, FromMin {1}, ToMin {2}", id, fromMin, toMin));
                    }
                }
                finally
                {
                    backend.Close();
                }
            }

            return result;
        }
        #endregion




    }
}
