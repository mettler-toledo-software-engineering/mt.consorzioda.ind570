﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using MT.DataServices.Logging;
using MT.DataServices.Plugins;
using MT.DataServices.Plugins.UnibaseEx.Backends;
using MT.DataServices.Plugins.UnibaseEx.Configuration;
using MT.DataServices.Plugins.UnibaseEx.Sql;
using MT.DataServices.Utilities;

namespace mt.consorzio.Plugin
{
    [Export(typeof(IPlugin))]
    public class ConsorzioPlugin : IPlugin
    {
        private IPluginHost _host;

        [Import]
        private ILogSink _log;

        public void Activate(IPluginHost host)
        {
            _host = host;
            host.StartServices += new EventHandler<ServicesEventArgs>(OnStartServices);
            host.StopServices += new EventHandler<ServicesEventArgs>(OnStopServices);
        }

        void OnStartServices(object sender, ServicesEventArgs e)
        {
            var unibaseExPluginConfig = _host.RuntimeConfiguration.HostConfiguration.PluginConfigurations.FirstOrDefault(d => d.PluginType == "MT.DataServices.Plugins.UnibaseEx.Plugin");
            if (unibaseExPluginConfig == null)
            {
                _log.Fatal("Cannot find UnibaseEx configuration.");
                return;
            }

            var unibaseExConfig = XNodeSerializer.Deserialize<UnibaseExConfiguration>(unibaseExPluginConfig.ConfigurationObject);
            if (unibaseExConfig == null)
            {
                _log.Fatal("UnibaseEx configuration invalid.");
                return;
            }

            var defaultEndpoint = unibaseExConfig.EndPoints.FirstOrDefault();
            if (defaultEndpoint == null)
            {
                _log.Fatal("No Endpoint registered.");
                return;
            }
        }

        void OnStopServices(object sender, ServicesEventArgs e)
        {
            ;
        }

        public IEnumerable<IPluginConfigurationPane> CreateConfigurationControl(IPluginConfigurationHost configurationHost)
        {
            yield break;
        }

        public string Manufacturer
        {
            get { return "Mettler Toledo (Schweiz) GmbH"; }
        }

        public string Name
        {
            get { return "MT.Consorzio"; }
        }

        public Version Version
        {
            get { return Assembly.GetExecutingAssembly().GetName().Version; }
        }

        public void Dispose()
        {
            ;
        }
    }
}
