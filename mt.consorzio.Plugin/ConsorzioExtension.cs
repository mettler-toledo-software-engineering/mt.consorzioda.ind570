﻿using System;
using System.ComponentModel.Composition;
using System.Linq;
using MT.DataServices.Plugins.UnibaseEx.Communication;

namespace mt.consorzio.Plugin
{
    [Export(typeof(IExtensionProvider))]
    public class ConsorzioExtension : IExtensionProvider
    {
        
        public object CreateExtensionObject(IEndPointHostingService endpointHost)
        {
            return new ConsorzioExtensionContext(endpointHost);
        }

        public string Name
        {
            get { return "ConsorzioExtension"; }
        }
    }
}
