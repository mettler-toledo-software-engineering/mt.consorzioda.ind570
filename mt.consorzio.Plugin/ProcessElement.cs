﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mt.consorzio.Plugin
{
    public class ProcessElement
    {
        public int TransactionId { get; set; }
        public string Transporter { get; set; }
        public string Vehicle { get; set; }
        public string TimeStamp { get; set; }
        public string Article { get; set; }
        public string Company { get; set; }
        public int ClientNr { get; set; }
        public int BolletinoNr { get; set; }
        public double FirstWeight { get; set; }
        public double SecondWeight { get; set; }
        
        //public double Weight { get; set; }
        //public string ScaleId { get; set; }
        //public bool LeaveOrder { get; set; }
        //public int LoafCount { get; set; }
    }
}
