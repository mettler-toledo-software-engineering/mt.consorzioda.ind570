﻿using System;
using System.Data;
using System.Linq;
using MT.DataServices.Plugins.UnibaseEx.Backends;
using MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree;

namespace mt.consorzio.Plugin
{
    internal static class DatabaseHelper
    {
        public static DataTable ToDataTable(this IRelationalResultSet result)
        {
            DataTable dt = new DataTable();
            dt.Load(((IDataRelationalResultSet)result).Reader);
            return dt;
        }

        public static object ToScalar(this IRelationalResultSet result)
        {
            using (result)
            {
                if (!result.Read())
                    return DBNull.Value;
                else
                    return result.Get(0);
            }
        }

        public static void ToNonQuery(this IRelationalResultSet result)
        {
            result.Dispose();
        }

        public static IRelationalResultSet ExecuteRaw(this IRelationalBackend backend, string commandText, params ParameterValue[] arguments)
        {
            var stmt = new StatementSequence(new ExecRawStatement(commandText));
            return backend.Execute(stmt, arguments);
        }
    }
}
