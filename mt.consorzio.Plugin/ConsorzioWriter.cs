﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading;
using MT.DataServices.Logging;
using MT.DataServices.Plugins;
using MT.DataServices.Plugins.UnibaseEx.Backends;
using MT.DataServices.Plugins.UnibaseEx.Configuration;
using MT.DataServices.Utilities;

namespace mt.consorzio.Plugin
{
    public class ConsorzioWriter : IService
    {
        private readonly Mutex procMutex;
        private readonly ProcessContainer processContainer;
        private readonly ILogSink _log;
        private readonly Func<IRelationalBackend> backendFactory;

        public ConsorzioWriter(Func<IRelationalBackend> backendFactory, ILogSink defaultLog, ProcessContainer processContainer)
        {
            this.backendFactory = backendFactory;
            _log = defaultLog;
            this.processContainer = processContainer;

            bool createdNew;
            var sid = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
            MutexSecurity sec = new MutexSecurity();
            sec.AddAccessRule(new MutexAccessRule(sid, MutexRights.FullControl, AccessControlType.Allow));
            procMutex = new Mutex(false, "Global\\DatabaseMutex", out createdNew, sec);
        }

        public void Run(IServiceHost host)
        {
            host.Log.Hint("Starting...");
            while (!host.CancellationToken.IsCancellationRequested)
            {
                Thread.Sleep(1000);
                //host.Log.Hint("Waiting...");
            }
        }

        public string Name
        {
            get { return "Consorzio Writer"; }
        }
    }
}
