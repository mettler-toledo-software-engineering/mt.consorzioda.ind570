﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;

namespace mt.consorzio.Plugin
{
    [Export(typeof(ProcessContainer))]
    public class ProcessContainer
    {
        private readonly Queue<ProcessElement> elementQueue = new Queue<ProcessElement>();
        private readonly Queue<Weighing> weighingQueue = new Queue<Weighing>();
        //private int loafCount;
        //private int aktLoafCount;
        private int _transactionId;
        //private string number;
        
        private string _transporter;
        private string _vehicle;
        private string _timeStamp;
        private string _article;
        private string _company;
        private int _clientNr;
        private int _bolletinoNr;
        private double _firstWeight;
        private double _secondWeight;


        public int WriteProcessElement(int transactionId, string transporter, string vehicle, string timeStamp,
            string article, string company, int clientNr, int bolletinoNr, double firstWeight, double secondWeight,
            bool leaveTransaction)
        {
            if ((leaveTransaction == true) 
            {
                var newProcessElement = new ProcessElement()
                {
                    TransactionId = transactionId,
                    Transporter = transporter,
                    Vehicle = vehicle,
                    TimeStamp = timeStamp,
                    Article = article,
                    Company = company,
                    ClientNr = clientNr,
                    BolletinoNr = bolletinoNr,
                    FirstWeight = firstWeight,
                    SecondWeight = secondWeight
                };


                lock (elementQueue)
                {
                    elementQueue.Enqueue(newProcessElement);
                    //if (leaveTransaction == false) aktLoafCount++;
                }

            }
            return 0;//loafCount - aktLoafCount;

        }

        public int InsertWeighing(string timeStamp, double grossWeight, double netWeight, double tareWeight, string unit)
        {
            var newWeiging = new Weighing()
            {
                TimeStamp = timeStamp,
                GrossWeight = grossWeight,
                NetWeight = netWeight,
                TareWeight = tareWeight,
                Unit = unit
            };

            lock (weighingQueue)
            {
                weighingQueue.Enqueue(newWeiging);
            }

            return 0;
        }

        public ProcessElement ReadProcessElement()
        {

            lock (elementQueue)
            {
                if (elementQueue.Count > 0)
                    return elementQueue.Dequeue();
                else
                    return null;
            }
        }

        public void BeginNewTransaction(int transactionId, string transporter, string vehicle, string timeStamp,
            string article, string company, int clientNr, int bolletinoNr, double firstWeight, double secondWeight)
        {

            _transactionId = transactionId;
            _transporter = transporter;
            _vehicle = vehicle;
            _timeStamp = timeStamp;
            _article = article;
            _company = company;
            _clientNr = clientNr;
            _bolletinoNr = bolletinoNr;
            _firstWeight = firstWeight;
            _secondWeight = secondWeight;

            //this.loafCount = loafCount;
            //this.aktLoafCount = aktLoafCount;
            //this.number = number;

        }

        public bool IsTransactionRunning
        {
            get { return _transactionId > 0; }
        }

        public int TransactionId
        {
            get { return _transactionId; }
        }

        //public string Number
        //{
        //    get { return number; }
        //}

        //public int AktLoafCount
        //{
        //    get { return aktLoafCount; }
        //}

        //public int LoafCount
        //{
        //    get { return loafCount; }
        //}

        public void CompleteTransaction()
        {
            _transactionId = 0;
        }

        public bool IsTransactionFinished
        {
            get
            {
                lock (elementQueue)
                {
                    // TODO add check
                    return true;//((aktLoafCount >= loafCount) && (elementQueue.Count == 0));
                }
            }

        }

    }
}
